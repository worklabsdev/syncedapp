import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { SignInComponent } from "./components/sign-in/sign-in.component";
import { SignUpComponent } from "./components/sign-up/sign-up.component";
import { OtpVerificationComponent } from "./components/otp-verification/otp-verification.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AddIncomeComponent } from "./components/add-income/add-income.component";
import { IncomeComponent } from "./components/income/income.component";
import { AddExpensesComponent } from "./components/add-expenses/add-expenses.component";
import { ExpensesComponent } from "./components/expenses/expenses.component";

const routes: Routes = [
    { path: "", redirectTo: "/dashboard", pathMatch: "full" },
    // { path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
    { path: "signin", component: SignInComponent },
    { path: "signup", component: SignUpComponent },
    { path: "otp", component: OtpVerificationComponent },
    { path: "addincome", component: AddIncomeComponent },
    { path: "dashboard", component: DashboardComponent },
    { path: "income", component: IncomeComponent },
    { path: "expenses", component: ExpensesComponent },
    { path: "addexpenses", component: AddExpensesComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule {}
