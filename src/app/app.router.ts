import { SignInComponent } from "./components/sign-in/sign-in.component";
import { HomeComponent } from "./home/home.component";

export const appRoutes: any = [
    { Path: "", component: HomeComponent },
    { Path: "SignIn", component: SignInComponent }
];

export const appComponents: any = [
    {
        SignInComponent,
        HomeComponent
    }
];
