import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { HttpModule } from "@angular/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SignInComponent } from "./components/sign-in/sign-in.component";
import { SignUpComponent } from "./components/sign-up/sign-up.component";
import { UserService } from "./providers/user.service";

import { NativeScriptHttpModule } from "nativescript-angular/http";
import { Http } from "@angular/http";
import {
    TranslateModule,
    TranslateLoader,
    TranslateStaticLoader
} from "ng2-translate";
import { NativescriptBottomNavigationModule } from "nativescript-bottom-navigation/angular";
// import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer";
import { OtpVerificationComponent } from "./components/otp-verification/otp-verification.component";
import { SideNavigationComponent } from "./components/navigation/side-navigation/side-navigation.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { IncomeComponent } from "./components/income/income.component";
import { AddIncomeComponent } from "./components/add-income/add-income.component";
import { ExpensesComponent } from "./components/expenses/expenses.component";
import { AddExpensesComponent } from "./components/add-expenses/add-expenses.component";
import { AuthHeaderComponent } from "./components/auth-header/auth-header.component";
import { SettingsComponent } from './components/settings/settings.component';

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, "/i18n", ".json");
}
@NgModule({
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        HttpModule,
        NativeScriptHttpModule,
        TranslateModule.forRoot([
            {
                provide: TranslateLoader,
                deps: [Http],
                useFactory: createTranslateLoader
            }
        ]),
        NativescriptBottomNavigationModule
    ],
    declarations: [
        AppComponent,
        SignInComponent,
        SignUpComponent,
        OtpVerificationComponent,
        SideNavigationComponent,
        DashboardComponent,
        IncomeComponent,
        AddIncomeComponent,
        ExpensesComponent,
        AddExpensesComponent,
        AuthHeaderComponent,
        SettingsComponent
    ],
    providers: [UserService],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {}
