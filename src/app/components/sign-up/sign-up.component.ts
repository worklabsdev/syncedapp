import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "./../../providers/user.service";
import { TranslateService } from "ng2-translate";
import * as Platform from "tns-core-modules/platform";
import {
    Feedback,
    FeedbackType,
    FeedbackPosition
} from "nativescript-feedback";
@Component({
    selector: "ns-sign-up",
    templateUrl: "./sign-up.component.html",
    styleUrls: ["./sign-up.component.css"],
    moduleId: module.id
})
export class SignUpComponent implements OnInit {
    private feedback: Feedback;
    public language: String;
    private emailcheck: Boolean = false;
    constructor(
        private router: Router,
        private userservice: UserService,
        private translate: TranslateService
    ) {
        this.feedback = new Feedback();
        this.language = Platform.device.language;
        this.translate.setDefaultLang("en");
        this.translate.use(Platform.device.language.split("-")[0]);
    }

    ngOnInit() {}
    navigateSignin() {
        console.log("you are in the navigatito n");
        this.router.navigate(["signin"]);
    }

    register(firstName, lastName, mobile, email, companyName, password) {
        firstName ? firstName.replace(/ /g, "").toLowerCase() : "";
        lastName ? lastName.replace(/ /g, "").toLowerCase() : "";
        mobile ? mobile.replace(/ /g, "").toLowerCase() : "";
        this.emailcheck = this.userservice.ValidateEmail(email);
        companyName ? companyName.replace(/ /g, "").toLowerCase() : "";
        password ? password.replace(/ /g, "") : "";

        if (
            firstName &&
            lastName &&
            mobile &&
            this.emailcheck &&
            companyName &&
            password
        ) {
            this.feedback.show({
                message: "Thank you for correct values",
                type: FeedbackType.Success
            });
            setTimeout(() => {
                this.router.navigate(["otp"]);
            }, 3000);
        } else if (
            !firstName ||
            !lastName ||
            !mobile ||
            !companyName ||
            !password
        ) {
            this.feedback.show({
                message: "Please enter required fields",
                type: FeedbackType.Error
            });
        } else if (this.emailcheck == false) {
            this.feedback.show({
                message: "Invalid Email",
                type: FeedbackType.Error
            });
        }
    }
}
