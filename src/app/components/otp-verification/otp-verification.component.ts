import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
@Component({
    selector: "ns-otp-verification",
    templateUrl: "./otp-verification.component.html",
    styleUrls: ["./otp-verification.component.css"],
    moduleId: module.id
})
export class OtpVerificationComponent implements OnInit {
    constructor(
        private router: Router,
        public routerExtensions: RouterExtensions
    ) {}

    ngOnInit() {}
    verify(otp) {
        console.log("this is opt" + otp);
        this.router.navigate(["dashboard"]);
    }
    onNavBtnTap() {
        this.routerExtensions.back();
    }
}
