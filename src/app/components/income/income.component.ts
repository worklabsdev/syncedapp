import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Router } from "@angular/router";
import { Fab } from "nativescript-floatingactionbutton";
import { registerElement } from "nativescript-angular/element-registry";
// registerElement("Fab", () => Fab);
import * as dialogs from "tns-core-modules/ui/dialogs";
import { RouterExtensions } from "nativescript-angular/router";
var Sqlite = require("nativescript-sqlite");

// import { registerElement } from "nativescript-angular/element-registry";
// registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);
// class Country {
//     constructor(public name: string) {}
// }

// let europianCountries = [
//     "Austria",
//     "Belgium",
//     "Bulgaria",
//     "Croatia",
//     "Cyprus",
//     "Czech Republic",
//     "Denmark",
//     "Estonia",
//     "Finland",
//     "France",
//     "Germany",
//     "Greece",
//     "Hungary",
//     "Ireland",
//     "Italy",
//     "Latvia",
//     "Lithuania",
//     "Luxembourg",
//     "Malta",
//     "Netherlands",
//     "Poland",
//     "Portugal",
//     "Romania",
//     "Slovakia",
//     "Slovenia",
//     "Spain",
//     "Sweden",
//     "United Kingdom"
// ];

@Component({
    selector: "ns-income",
    templateUrl: "./income.component.html",
    styleUrls: ["./income.component.css"],
    moduleId: module.id
})
export class IncomeComponent implements OnInit {
    // public countries: Array<Country>;
    public income: Array<any>;

    constructor(
        private router: Router,
        public routerExtensions: RouterExtensions
    ) {
        // this.countries = [];
        // for (let i = 0; i < europianCountries.length; i++) {
        //     this.countries.push(new Country(europianCountries[i]));
        // }
    }

    ngOnInit() {
        this.fetch();
    }

    public onItemTap(args) {
        console.log("Item Tapped at cell index: " + args.index);
    }
    handleInvestment() {
        dialogs
            .action({
                message: "Your message",
                cancelButtonText: "Cancel text",
                actions: ["Add Investment", "Add Expenses"]
            })
            .then(result => {
                console.log("Dialog result: " + result);
                if (result == "Add Investment") {
                    alert("Add Investment");
                    //Do action1
                } else if (result == "Add Expenses") {
                    //Do action2
                    alert("Add Expenses");
                }
            });
    }
    fetch() {
        //
        this.income = [];
        var kk = [];
        new Sqlite("syncedaccounts.db", (err, db) => {
            db.all(
                "SELECT * FROM enteries WHERE inventoryType = 'INCOME' AND status = 0"
            ).then(
                rows => {
                    console.log("this is synced data", rows);
                    rows.forEach(element => {
                        if (element[2] == "INCOME") {
                            var jj = {
                                id: element[0],
                                title: element[1],
                                inventoryType: element[2],
                                status: element[3],
                                description: element[4],
                                amount: element[5],
                                createdAt: Date.parse(element[6])
                            };
                        }
                        this.income.push(jj);
                    });
                },
                error => {
                    console.log("getting synced inventory giving error", error);
                }
            );
        });
    }

    addIncomeNavigation() {
        this.router.navigate(["addincome"]);
    }
    onNavBtnTap() {
        this.routerExtensions.back();
    }
}
