import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ns-auth-header',
  templateUrl: './auth-header.component.html',
  styleUrls: ['./auth-header.component.css'],
  moduleId: module.id,
})
export class AuthHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
