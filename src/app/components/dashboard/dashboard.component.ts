import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { Fab } from "nativescript-floatingactionbutton";
import { registerElement } from "nativescript-angular/element-registry";
registerElement("Fab", () => Fab);
import { Router } from "@angular/router";
import * as dialogs from "tns-core-modules/ui/dialogs";
var Sqlite = require("nativescript-sqlite");
var view = require("ui/core/view");
import {
    BottomNavigation,
    BottomNavigationTab,
    OnTabPressedEventData,
    OnTabSelectedEventData
} from "nativescript-bottom-navigation";

@Component({
    selector: "ns-dashboard",
    templateUrl: "./dashboard.component.html",
    styleUrls: ["./dashboard.component.css"],
    changeDetection: ChangeDetectionStrategy.OnPush,
    moduleId: module.id
})
export class DashboardComponent implements OnInit {
    public income: Array<any>;
    public drawer: any;
    constructor(private router: Router) {}

    ngOnInit() {
        this.fetch();
        this.pageLoaded;
    }
    public onItemTap(args) {
        console.log("Item Tapped at cell index: " + args.index);
    }
    handleInvestment() {
        dialogs
            .action({
                message: "Your message",
                cancelButtonText: "Cancel",
                actions: ["Add Investment", "Add Expenses"]
            })
            .then(result => {
                console.log("Dialog result: " + result);
                if (result == "Add Investment") {
                    this.router.navigate(["addincome"]);
                } else if (result == "Add Expenses") {
                    this.router.navigate(["addexpenses"]);
                    //Do action2
                    // alert("Add Expenses");
                }
            });
    }

    public tabs: BottomNavigationTab[] = [
        new BottomNavigationTab("First", "ic_home"),
        new BottomNavigationTab("Second", "ic_view_list"),
        new BottomNavigationTab("Third", "ic_menu")
    ];
    onBottomNavigationTabPressed(args: OnTabPressedEventData): void {
        console.log(`Tab pressed:  ${args.index}`);
    }

    onBottomNavigationTabSelected(args: OnTabSelectedEventData): void {
        console.log(`Tab selected:  ${args.oldIndex}`);
    }
    handlenavigation(screen) {
        if (screen == "income") {
            this.router.navigate(["addincome"]);
        } else {
            this.router.navigate(["addexpenses"]);
        }
    }
    fetch() {
        this.income = [];
        var kk = [];
        new Sqlite("syncedaccounts.db", (err, db) => {
            db.all("SELECT * FROM enteries WHERE status = 0").then(
                rows => {
                    console.log("this is synced data", rows);
                    rows.forEach(element => {
                        // if (element[2] == "INCOME") {
                        var jj = {
                            id: element[0],
                            title: element[1],
                            inventoryType: element[2],
                            status: element[3],
                            description: element[4],
                            amount: element[5],
                            createdAt: Date.parse(element[6])
                        };
                        // }

                        this.income.push(jj);
                    });
                },
                error => {
                    console.log("getting synced inventory giving error", error);
                }
            );
        });
    }

    pageLoaded = function(args) {
        var page = args.object;
        this.drawer = view.getViewById(page, "sideDrawer");
    };

    toggleDrawer = function() {
        this.drawer.toggleDrawerState();
    };
}
