import {
    Component,
    ViewChild,
    OnInit,
    AfterViewInit,
    ChangeDetectorRef
} from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from "ng2-translate";
import * as Platform from "tns-core-modules/platform";
import {
    Feedback,
    FeedbackType,
    FeedbackPosition
} from "nativescript-feedback";
import { UserService } from "../.././providers/user.service";

@Component({
    selector: "ns-sign-in",
    templateUrl: "./sign-in.component.html",
    styleUrls: ["./sign-in.component.css"],
    moduleId: module.id
})
export class SignInComponent implements OnInit {
    private feedback: Feedback;
    public language: String;

    constructor(
        private router: Router,
        private userservice: UserService,
        private translate: TranslateService
    ) {
        this.feedback = new Feedback();
        this.language = Platform.device.language;
        this.translate.setDefaultLang("en");
        this.translate.use(Platform.device.language.split("-")[0]);
    }

    ngOnInit() {}

    authenticate(email, pass) {
        if (email && pass) {
            this.userservice.ValidateEmail(email);
            this.router.navigate(["dashboard"]);
            console.log("you passed" + email);
        } else {
            this.feedback.show({
                message: "Please enter required fields",
                type: FeedbackType.Error
            });
        }
    }
    navigateSignup() {
        console.log("you are in the navigatito n");
        this.router.navigate(["signup"]);
    }
}
