import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "./../../providers/user.service";
import { TranslateService } from "ng2-translate";
import * as Platform from "tns-core-modules/platform";
import { RouterExtensions } from "nativescript-angular/router";
import * as imageSourceModule from "tns-core-modules/image-source";
var fs = require("file-system");
let imagePicker = require("nativescript-imagepicker");

// import { Button } from "tns-core-modules/ui/button";
// import { Page } from "tns-core-modules/ui/page";
// import { EventData } from "tns-core-modules/data/observable";
var Sqlite = require("nativescript-sqlite");
import {
    Feedback,
    FeedbackType,
    FeedbackPosition
} from "nativescript-feedback";
@Component({
    selector: "ns-add-income",
    templateUrl: "./add-income.component.html",
    styleUrls: ["./add-income.component.css"],
    moduleId: module.id
})
export class AddIncomeComponent implements OnInit {
    private feedback: Feedback;
    public language: String;
    private emailcheck: Boolean = false;
    private database: any;
    public income: Array<any>;
    public myImage = "";
    constructor(
        private router: Router,
        private userservice: UserService,
        private translate: TranslateService,
        private routerExtensions: RouterExtensions // public args: EventData
    ) {
        this.feedback = new Feedback();
        this.language = Platform.device.language;
        // ========================= code for the translation pipe ================
        this.translate.setDefaultLang("en");
        this.translate.use(Platform.device.language.split("-")[0]);
        // ==========================================================

        // ========================= code for the
        new Sqlite("syncedaccounts.db").then(
            db => {
                db.execSQL(
                    "CREATE TABLE IF NOT EXISTS enteries(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT,inventoryType TEXT, status BOOLEAN, description TEXT, amount DOUBLE,createdAt DATETIME DEFAULT CURRENT_TIMESTAMP)"
                ).then(
                    id => {
                        this.database = db;
                        console.log(
                            "this is the database that is created",
                            this.database
                        );
                    },
                    error => {
                        console.log("ERROR while creating table", error);
                    }
                );
            },
            error => {
                console.log("ERROR while connecting to db", error);
            }
        );
    }
    ngOnInit() {
        // var dd = this.userservice.fetch();
        // console.log("DDDDDDD", dd)/;
    }
    public addIncome(title, amount, description) {
        this.database
            .execSQL(
                "INSERT INTO enteries (title,inventoryType,status,description,amount) VALUES (?,?,?,?,?) ",
                [title, "INCOME", 0, description, amount]
            )
            .then(
                id => {
                    this.feedback.show({
                        message: "Income saved locally",
                        type: FeedbackType.Success
                    });
                    console.log("DATA SUCCESSFULLY SAVED", id);
                    setTimeout(() => {
                        this.router.navigate(["dashboard"]);
                    }, 2500);
                    // alert("Income saved locally");
                },
                error => {
                    this.feedback.show({
                        message: "Error! Saving income locally",
                        type: FeedbackType.Error
                    });
                    console.log("ERROR WHITLE SAVING DATA", error);
                    // alert("Error ! while creating database");
                }
            );
    }
    onNavBtnTap() {
        this.routerExtensions.back();
    }
    getPicturefromgallery() {
        var milisecond = new Date().getTime();
        var that = this;
        let context = imagePicker.create({
            mode: "single"
        });
        context
            .authorize()
            .then(() => {
                return context.present();
            })
            .then(selection => {
                selection.forEach(selected => {
                    // let file;
                    // if(selected._android) {
                    //     file = fileSystemModule.
                    // }

                    console.log("SELECTED", selected);
                    imageSourceModule
                        .fromAsset(selected)
                        .then(function(imagesource) {
                            let folder = fs.knownFolders.documents();
                            var path = fs.path.join(
                                folder.path,
                                milisecond + ".png"
                            );
                            var saved = imagesource.saveToFile(path, "png");
                            console.log("the images is savedx", saved);
                            that.myImage = path;
                            console.log("the images is savedx", that.myImage);
                        });
                });
            });
    }
}
