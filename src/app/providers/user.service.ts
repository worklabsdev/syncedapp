import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs";
import { map } from "rxjs/operators";
var Sqlite = require("nativescript-sqlite");
import {
    Feedback,
    FeedbackType,
    FeedbackPosition
} from "nativescript-feedback";
@Injectable()
export class UserService {
    private feedback: Feedback;
    private database: any;
    public income: Array<any>;
    constructor(http: Http) {
        this.feedback = new Feedback();
        this.income = [];
        // new Sqlite("synced.db").then(
        //     db => {
        //         db.execSQL(
        //             "CREATE TABLE IF NOT EXISTS syncedinventory(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, inventoryType TEXT, description TEXT,amount DOUBLE)"
        //         ).then(
        //             id => {
        //                 this.database = db;
        //                 console.log("HITSI IS DB", db);
        //             },
        //             error => {
        //                 console.log("CREATE TABLE ERROR", error);
        //             }
        //         );
        //     },
        //     error => {
        //         console.log("OPEN DB ERROR", error);
        //     }
        // );
    }

    public addIncome(title, amount, description) {
        this.database
            .execSQL(
                "INSERT INTO syncedinventory (title,inventoryType,description,amount) VALUES (?,?,?,?) ",
                ["title", "INCOME", "description", 400]
            )
            // .pipe(map(res => res));
            .then(
                id => {
                    console.log("DATA SUCCESSFULLY SAVED", id);
                },
                error => {
                    console.log("ERROR WHITLE SAVING DATA", error);
                }
            );
    }

    public async fetch() {
        var kk = [];

        await this.database.all("SELECT * FROM syncedinventory").then(
            rows => {
                this.income = [];
                console.log("this is synced data", rows);
                kk = rows.map(element => ({
                    id: element[0],
                    title: element[1],
                    description: element[3],
                    amount: element[6]
                }));
                console.log("this is the UnSTRUCTURED data*****", rows);
                console.log("this is the structured data", kk);
                // return kk;
                // console.log("KKKKKKKKKKK", kk);
            },
            error => {
                console.log("getting synced inventory giving error", error);
            }
        );
        return kk;
    }

    ValidateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return true;
        } else {
            this.feedback.show({
                message: "Invalid Email",
                type: FeedbackType.Error
            });
            return false;
        }
    }
}
